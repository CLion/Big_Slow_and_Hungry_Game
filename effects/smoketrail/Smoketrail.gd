extends Line2D

@export var limited_lifetime := false
@export var wildness := 3.0
@export var min_spawn_distance := 5.0

var lifetime := [1.2, 1.6]
var tick_speed := 0.05
var tick := 0.0
var wild_speed := 0.1
var point_age := [0.0]
var stopped := false

@onready var tween := $Decay

func _ready():
	set_as_top_level(true)
	clear_points()
	if limited_lifetime:
		stop()


func stop():
	stopped = true
	tween.interpolate_property(self, "modulate:a", 1.0, 0.0, randf_range(lifetime[0], lifetime[1]), Tween.TRANS_CIRC, Tween.EASE_OUT)
	tween.start()


func _process(delta):
	if tick > tick_speed:
		tick = 0
		for p in range(get_point_count()):
			point_age[p] += 5*delta
			var rand_vector := Vector2( randf_range(-wild_speed, wild_speed), randf_range(-wild_speed, wild_speed) )
			points[p] += rand_vector * wildness * point_age[p]
	else:
		tick += delta


func add_point(point_pos:Vector2, at_pos := -1):
	if get_point_count() > 0 and point_pos.distance_to( points[get_point_count()-1] ) < min_spawn_distance:
		return
	point_age.append(0.0)
	super.add_point(point_pos, at_pos)


