extends Camera2D

var zoom_incr = 10.0/12.0

func _unhandled_input(event) -> void:
	var mbutton := event as InputEventMouseButton
	var mmotion :InputEventMouseMotion = event as InputEventMouseMotion
	
	if Input.is_mouse_button_pressed(3):
		if mmotion:
			global_position -= mmotion.relative * zoom
	if mbutton:
		if Input.is_mouse_button_pressed(4):
			global_position += (get_global_mouse_position() - global_position) * (1 - zoom_incr)
			set_zoom(zoom * zoom_incr)
		if Input.is_mouse_button_pressed(5):
			global_position += (get_global_mouse_position() - global_position) * (1 - 1/zoom_incr)
			set_zoom(zoom / zoom_incr)

