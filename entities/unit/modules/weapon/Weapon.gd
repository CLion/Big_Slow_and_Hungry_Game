extends Node2D

@onready var ProjectileScene = preload("res://entities/projectile/Projectile.tscn")

var inaccuracy = 0.1

var team = ""

var target_node_path = null
var target
var shooting = false
var range_of_fire = 1500
var status = null
var spurt_size = 3
var spurt_left = spurt_size
var spurt_cadence = 0.05
var spurt_cooldown = 2
var rot_speed_max = 0.2
var scan_rate = 1

enum {INIT, ACTIVE, DEACTIVATED}

func _ready():
	change_status(INIT)
	$CadenceTimer.wait_time = spurt_cadence
	$CooldownTimer.wait_time = spurt_cooldown
	$ScanTimer.wait_time = scan_rate

func _process(delta):
	match status:
		ACTIVE:
			var target_exists = target != null# and has_node(target_node_path)
			var target_out_of_range = true
			#
			var forward_vector = Vector2.RIGHT.rotated(global_rotation)
			var target_vector = forward_vector
			var rot_dir = 0
			if target_exists:
				var rel_target_pos = target.global_position-global_position
				if rel_target_pos.length() <= range_of_fire:
					target_out_of_range = false
					target_vector = rel_target_pos
				rot_dir = forward_vector.angle_to(target_vector)
			else:
				rot_dir = -rotation
			#
			rotate(clamp(rot_dir, -rot_speed_max, rot_speed_max))
			#
			if not target_exists or target_out_of_range:
				set_target(null)
				scan_for_enemies()

func start():
	$ScanTimer.start()
	$ScanArea/CollisionShape2D.shape.radius = range_of_fire
	change_status(ACTIVE)

func change_status(new_status):
	match new_status:
		INIT:
			pass
		ACTIVE:
			pass
		DEACTIVATED:
			pass
	status = new_status

func set_target(new_target):
	if new_target != null:
		target = new_target
		target_node_path = target.get_path()
		start_shooting()
	else:
		target = null
		target_node_path = null
		stop_shooting()

func start_shooting():
	shooting = true
	if $CooldownTimer.is_stopped():
		$CooldownTimer.start()
	
func stop_shooting():
	shooting = false

func shoot():
	spurt_left -= 1
	var projectile =  ProjectileScene.instantiate()
	get_node("/root/Map/projectiles").add_child(projectile)
	var inaccuracy_factor = (1+ (2*randf() - 1)*inaccuracy)
	projectile.start(team,range_of_fire,$Muzzle.global_position,$Muzzle.global_rotation * inaccuracy_factor)

func _on_CooldownTimer_timeout():
	if shooting:
		spurt_left = spurt_size
		$CadenceTimer.start()
		
func _on_CadenceTimer_timeout():
	if status == ACTIVE and shooting and spurt_left > 0:
		$MuzzleFlash.visible = true
		shoot()
		$CadenceTimer.start()
	else:
		$MuzzleFlash.visible = false
		$CooldownTimer.start()	

func _on_ScanTimer_timeout():
	scan_for_enemies()

func scan_for_enemies():
	var areas_in_vision = $ScanArea.get_overlapping_areas()
	var enemies_in_vision = Array()
	for area_i in areas_in_vision:
		if not area_i.is_in_group("team_"+team) and area_i.is_in_group("units"):
			enemies_in_vision.append(area_i)
	if enemies_in_vision.is_empty():
		$ScanTimer.start()
	else:
		var nearest_enemy = enemies_in_vision[0]
		var distance_min = (nearest_enemy.position - position).length()
		for enemy_i in enemies_in_vision.slice(1,100):
			var distance_i = (enemy_i.position - position).length()
			if distance_i < distance_min:
				nearest_enemy = enemy_i
				distance_min = distance_i
		set_target(nearest_enemy)

	
