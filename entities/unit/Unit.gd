extends Area2D

@export var acc := 50.0
@export var brake_factor := 5.0
@export var speed_max := 1000.0
@export var rot_acc := 10.0
@export var rot_speed_max := 5.0

var circling_rot = (randi()%2-0.5)*0.9 * (0.5 + randf())
var ress_value = 1
var status = null
var command: Dictionary
var waypoint: Vector2
var speed = 0.0
var enemy_dir = null
var hold_speed = false
var approaching_stop = false
var rot_speed = 0.0
var unit_size = 50
var command_queue = null
var command_number = 0
var time_expired = false
var team: String = ""
var enemies_in_vision = Dictionary()
var hit_points = 10
var retreat_command = Dictionary()
var modules = Dictionary()
var has_weapon = false

enum {INIT, ACTIVE, DESTROYED}

signal recycle
signal destroyed

func _ready():
	_change_status(INIT)

func _process(delta):
	var rot_dir = 0
	var target_speed = 0
	#
	match status:
		ACTIVE:
			check_command_completed()
			#
			if command == null:
				return
			#
			var target_vector = Vector2.ZERO
			#
			target_vector = waypoint-position
			#
			rot_dir = _get_rot_dir(target_vector)
			target_speed = _get_target_speed(rot_dir, target_vector)
		DESTROYED:
			rot_dir = circling_rot
	#
	_apply_rot_speed(delta, rot_dir)
	_apply_speed(delta, target_speed)

func _change_status(new_status):
	match new_status:
		INIT:
			pass
		ACTIVE:
			pass
		DESTROYED:
			emit_signal("destroyed")
			$ExplosionTimer.start()
			$AnimationPlayer.play("explode")
	status = new_status

func start(_team, start_pos, start_speed, start_rot, 
		module_scenes, _command_queue, _retreat_command):
	set_team(self, _team)
	load_modules(module_scenes)
	position = start_pos
	speed = start_speed
	rotation = start_rot
	command_queue = _command_queue
	retreat_command = _retreat_command
	_change_status(ACTIVE)
	read_next_command()

func load_modules(module_scenes: Array):
	for i in module_scenes.size():
		var Module = module_scenes[i].instantiate()
		if Module.is_in_group("weapons"):
			has_weapon = true
		$Modules.add_child(Module)
		var module_pos = $ModulePositions.get_child(i)
		Module.position = module_pos.position
		Module.rotation = module_pos.rotation
		Module.start()
		set_team(Module, team)

func set_team(node: Node, _team):
	team = _team
	var color = Color(team)
	if color == Color.BLACK:
		color = Color.WHITE
	#
	node.get_node("Sprite2D").modulate = color
	node.add_to_group("team_" + team)

func _get_rot_dir(target_vector) -> float:
	var rot_dir = 0
	match command["type"]:
		"MOVE":
			rot_dir = Vector2.RIGHT.rotated(global_rotation) \
				.angle_to(target_vector)
			if abs(rot_dir) <= pow(rot_speed, 2) / (2*rot_acc):
				rot_dir = 0
			if has_weapon and command["follow_enemies"] and \
					$Modules/Weapon.target != null:
				rot_dir = circling_rot
		"WAIT":
			rot_dir = circling_rot
	return rot_dir

func _apply_rot_speed(delta, rot_dir):
	rot_speed = move_toward(rot_speed, 
		clamp(rot_dir, -1, 1)*rot_speed_max, rot_acc * delta)
	rotate(rot_speed * delta)

func _get_target_speed(rot_dir, target_vector) -> float:
	var target_speed = 0
	match command["type"]:
		"MOVE":
			if command["stop"] and target_vector.length() - unit_size/2.0 \
					<= pow(speed, 2) / (2*brake_factor*acc):
				target_speed = 0
			else:
				if abs(rot_dir) > 1:
					target_speed = 0
				else:
					target_speed = speed_max
		"WAIT":
			if command["hold_speed"]:
				target_speed = 0.5 * speed_max
	return target_speed

func _apply_speed(delta, target_speed):
	var delta_speed = acc * delta
	if target_speed < speed:
		delta_speed *= brake_factor
	speed = move_toward(speed, target_speed, delta_speed)
	#
	speed = clamp(speed, 0, speed_max)
	translate(speed * Vector2.RIGHT.rotated(rotation) * delta)

func _get_attack_move_target_vector() -> Vector2:
	return Vector2.RIGHT.rotated($Modules/Weapon.global_rotation+0.1)

func check_command_completed():
	if status != ACTIVE or command == null:
		return
	#
	var complete = false
	match command["type"]:
		"MOVE":
			if (waypoint-position).length() <= unit_size*0.5:
				complete = true
		"WAIT":
			if time_expired:
				complete = true
	#
	if complete:
		if "signal" in command.keys():
			match command["signal"]:
				"recycle":
					var spawner = instance_from_id(command["waypoint_id"])
					spawner.recycle_unit(ress_value)
					queue_free()
		#
		read_next_command()

func read_next_command():
	#
	if status != ACTIVE:
		return
	#
	command_number += 1
	if command_number >= len(command_queue):
		command_number = len(command_queue)
		apply_command(retreat_command)
	else:
		apply_command(command_queue[command_number])
	
func apply_command(new_command):
	var default_values = {"stop": false, "hold_speed": true, "follow_enemies": false}
	command = new_command
	command.merge(default_values)
	#
	match command["type"]:
		"MOVE":
			approaching_stop = command["stop"]
			if "waypoint_id" in command.keys():
				command["waypoint"] = instance_from_id(command["waypoint_id"]) \
					.position
			set_waypoint(command["waypoint"])
		"WAIT":
			hold_speed = command["hold_speed"]
			time_expired = false
			$WaitTimer.start(command["time"])
			
func set_waypoint(new_waypoint):
	waypoint = new_waypoint

func _on_WaitTimer_timeout():
	time_expired = true
				
func start_shooting():
	$Modules/Weapon.start_shooting()

func hit(damage):
	match status:
		ACTIVE:
			hit_points -= damage
			if hit_points <= 0:
				_change_status(DESTROYED)

func _on_ExplosionTimer_timeout():
	match status:
		DESTROYED:
			queue_free()
