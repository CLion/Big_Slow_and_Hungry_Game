extends Node2D

var unit_name: String = ""
var UnitScene: PackedScene = null

@onready var WeaponScene = preload("res://entities/unit/modules/weapon/Weapon.tscn")

@export var module_scenes: Array

@export var team := ""

var ress = 0
var ress_max = 20

func _ready():
	var _module_scenes = []
	for module in module_scenes:
		if module == "weapon":
			_module_scenes.append(WeaponScene)
	module_scenes = _module_scenes
	set_team(team)
	preload_unit()
	z_index = 1
	set_ress(ress_max)

func set_team(_team):
	team = _team
	var color = Color(team)
	if color == Color.BLACK:
		color = Color.WHITE
	$Sprite2D.modulate = color
	#
	add_to_group("team_" + team)
	
func preload_unit():
	if unit_name == "":
		UnitScene = preload("res://entities/unit/Unit.tscn")

func _on_SpawnTimer_timeout():
	if ress > 0:
		spawn_unit()

func recycle_unit(ress_value):
	set_ress(ress + ress_value)

func spawn_unit():
	var unit = UnitScene.instantiate()
	if ress < unit.ress_value:
		return
	else:
		set_ress(ress - unit.ress_value)
	get_node("/root/Map/units").add_child(unit)
	var path = []
	path.append({
		"type": "MOVE",
		"waypoint": $SpawnPosition.global_position + \
			100 * Vector2.RIGHT.rotated($SpawnPosition.global_rotation)
	})
	for target in get_tree().get_nodes_in_group("waypoints"):
		if target.is_in_group("team_" + team):
			var command = {
				"type": "MOVE",
				"waypoint_id": target.get_instance_id(),
				"follow_enemies": target.is_in_group("attack_commands")
			}
			path.append(command)
	path.append({
		"type": "WAIT",
		"time": 3,
		"hold_speed": true
	})
	var retreat_command = {
		"type": "MOVE",
		"waypoint_id": get_instance_id(),
		"signal": "recycle"
	}
	unit.start(team, $SpawnPosition.global_position, unit.speed_max*0.5, 
		$SpawnPosition.global_rotation, module_scenes, path, retreat_command)

func _on_RessProductionTimer_timeout():
	set_ress(ress+1)
	
func set_ress(_ress):
	ress = clamp(_ress, 0, ress_max)
	$RessCounter.text = str(ress)
