extends Area2D

@export var team := ""

func _ready():
	set_team(team)
	
func set_team(_team):
	team = _team
	var color = Color(team)
	if color == Color.BLACK:
		color = Color.WHITE
	$Sprite2D.modulate = color
	#
	add_to_group("team_" + team)
