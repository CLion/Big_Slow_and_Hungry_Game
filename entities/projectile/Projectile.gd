extends Node2D

var speed = 10000
var distance_travelled = 0
var distance_max = 10000
var team = ""
var status = null
var damage = 2

var hit_obj = null

enum {INIT, ACTIVE, EXPLODE}

func _ready():
	change_status(INIT)
	$Line2d.points.append(Vector2(-5, 0))
	$Line2d.points.append(Vector2(-speed * 0.1, 0))
	
func start(_team, range_of_fire, start_pos, start_rot):
	team = _team
	add_to_group("team_" + team)
	distance_max = range_of_fire*1.1
	position = start_pos
	rotation = start_rot
	change_status(ACTIVE)
	
func _process(delta):
	var old_position = position
	match status:
		ACTIVE:
			var travel_distance = speed * delta
			distance_travelled += travel_distance
			if distance_travelled > distance_max:
				travel_distance = distance_travelled - distance_max
			var velocity = Vector2.RIGHT.rotated(rotation) * travel_distance
			#
			var ray = PhysicsRayQueryParameters2D.create(global_position, 
				global_position + velocity, 1, [RID(self)])
			var result = get_world_2d().direct_space_state.intersect_ray(ray)
			if not result.is_empty() \
					and result["collider"].is_in_group("units") \
					and not result["collider"].is_in_group("team_" + team):
				result["collider"].hit(damage)
				$Explosion.visible = true
				position = result["position"]
				explode()
			else:
				translate(velocity)
				if distance_travelled >= distance_max:
					explode()

func change_status(new_status):
	if new_status == status:
		return
	match new_status:
		INIT:
			$Sprite2D.visible = false
			$Explosion.visible = false
		ACTIVE:
			$Sprite2D.visible = true
			$Explosion.visible = false
		EXPLODE:
			$Sprite2D.visible = false
			#$Explosion.visible = true
			$VanishTimer.start(0.1)
	status = new_status

func explode():
	change_status(EXPLODE)

func _on_VanishTimer_timeout():
	queue_free()
